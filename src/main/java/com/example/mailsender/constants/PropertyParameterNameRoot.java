package com.example.mailsender.constants;

public abstract class PropertyParameterNameRoot
{
	private PropertyParameterNameRoot()
	{
	}

	public static final String PARAM_ROOT = "mailsender.";
}
