package com.example.mailsender.constants.enums;

import com.example.mailsender.constants.MailConstants;

public enum MailType
{
	WELCOME( MailTemplate.WELCOME, MailConstants.WELCOME_PARAMS ),
	FORGOT_PASSWORD( MailTemplate.FORGOT_PASSWORD, MailConstants.FORGOT_PASSWORD_PARAMS ),
	WEEKLY_NEWSLETTER( MailTemplate.WEEKLY_NEWSLETTER, MailConstants.NEWSLETTER_PARAMS );

	public final MailParameterType[] params;
	public final MailTemplate template;

	MailType( MailTemplate template, MailParameterType... params )
	{
		this.template = template;
		this.params = params;
	}
}
