package com.example.mailsender.constants.enums;

import com.example.mailsender.constants.MailConstants;

public enum MailParameterType
{
	FIRSTNAME( MailConstants.FIRSTNAME ),
	LASTNAME( MailConstants.LASTNAME ),
	PASSWORD_RESET_URL( MailConstants.PASSWORD_RESET_URL ),
	NEWSLETTER( MailConstants.NEWSLETTER ),
	NEWSLETTER_DATE( MailConstants.NEWSLETTER_DATE );

	public final String parameter;

	MailParameterType( String param )
	{
		this.parameter = param;
	}
}
