package com.example.mailsender.constants.enums;

import com.example.mailsender.constants.MailConstants;

public enum MailTemplate
{
	WELCOME( MailConstants.WELCOME_TITLE, MailConstants.WELCOME_CONTENT ),
	FORGOT_PASSWORD( MailConstants.FORGOT_PASSWORD_TITLE, MailConstants.FORGOT_PASSWORD_CONTENT ),
	WEEKLY_NEWSLETTER( MailConstants.NEWSLETTER_TITLE, MailConstants.NEWSLETTER_CONTENT );

	public final String title;
	public final String content;

	MailTemplate( String title, String template )
	{
		this.title = title;
		this.content = template;
	}
}
