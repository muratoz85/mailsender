package com.example.mailsender.constants;

import com.example.mailsender.constants.enums.MailParameterType;

public abstract class MailConstants
{
	private MailConstants()
	{
	}

	private static final String FIRSTNAME_RAW = "FIRSTNAME";
	private static final String LASTNAME_RAW = "LASTNAME";
	private static final String RESET_URL_RAW = "PASSWORD_RESET_URL";
	private static final String NEWSLETTER_RAW = "NEWSLETTER";
	private static final String NEWSLETTER_DATE_RAW = "NEWSLETTER_DATE";

	private static final String PREFIX = "#{";
	private static final String SUFFIX = "}";

	public static final String FIRSTNAME = parametrizeForTemplating( FIRSTNAME_RAW );
	public static final String LASTNAME = parametrizeForTemplating( LASTNAME_RAW );
	public static final String PASSWORD_RESET_URL = parametrizeForTemplating( RESET_URL_RAW );
	public static final String NEWSLETTER = parametrizeForTemplating( NEWSLETTER_RAW );
	public static final String NEWSLETTER_DATE = parametrizeForTemplating( NEWSLETTER_DATE_RAW );

	private static String parametrizeForTemplating( String rawParam )
	{
		return PREFIX + rawParam + SUFFIX;
	}

	public static final String WELCOME_TITLE = "Hoşgeldiniz!";
	public static final String FORGOT_PASSWORD_TITLE = "Şifre yenileme";
	public static final String NEWSLETTER_TITLE = FIRSTNAME + ", " + NEWSLETTER_DATE + " tarihli bültenimizi kaçırma";

	public static final String WELCOME_CONTENT = "Sayın " + MailConstants.FIRSTNAME + " " + MailConstants.LASTNAME + ", aramıza hoşgeldiniz.";
	public static final String FORGOT_PASSWORD_CONTENT = "Şifrenizi yenilemek için aşağıdaki linke tıklayınız\n" + PASSWORD_RESET_URL;
	public static final String NEWSLETTER_CONTENT = "Merhaba " + FIRSTNAME + " " + LASTNAME + ";\n" + NEWSLETTER;

	public static final MailParameterType[] WELCOME_PARAMS = { MailParameterType.FIRSTNAME, MailParameterType.LASTNAME };
	public static final MailParameterType[] FORGOT_PASSWORD_PARAMS = { MailParameterType.PASSWORD_RESET_URL };
	public static final MailParameterType[] NEWSLETTER_PARAMS = { MailParameterType.FIRSTNAME, MailParameterType.LASTNAME,
																  MailParameterType.NEWSLETTER_DATE, MailParameterType.NEWSLETTER };
}
