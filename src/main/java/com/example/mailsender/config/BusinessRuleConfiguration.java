package com.example.mailsender.config;

import com.example.mailsender.config.intf.IBusinessRuleConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import static com.example.mailsender.constants.PropertyParameterNameRoot.PARAM_ROOT;

@Configuration
public class BusinessRuleConfiguration implements IBusinessRuleConfiguration
{
	private static final String SEND_TRIAL_THRESHOLD = "${" + PARAM_ROOT + "sendTrialCountThreshold}";

	@Value( SEND_TRIAL_THRESHOLD )
	private Integer sendTrialCountThreshold;

	@Override
	public Integer getSendTrialCountThreshold()
	{
		return sendTrialCountThreshold;
	}
}
