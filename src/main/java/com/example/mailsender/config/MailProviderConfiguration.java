package com.example.mailsender.config;

import com.example.mailsender.config.intf.IMailProviderConfiguration;
import com.example.mailsender.vo.MailProviderVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

import static com.example.mailsender.constants.PropertyParameterNameRoot.PARAM_ROOT;

@Configuration
public class MailProviderConfiguration implements IMailProviderConfiguration
{
	private static final String MAIL_PROVIDER_A_USERNAME = "${" + PARAM_ROOT + "mailProvider.A.username}";
	private static final String MAIL_PROVIDER_A_PASSWORD = "${" + PARAM_ROOT + "mailProvider.A.password}";
	private static final String MAIL_PROVIDER_A_ENDPOINT = "${" + PARAM_ROOT + "mailProvider.A.password}";

	private static final String MAIL_PROVIDER_B_USERNAME = "${" + PARAM_ROOT + "mailProvider.B.username}";
	private static final String MAIL_PROVIDER_B_PASSWORD = "${" + PARAM_ROOT + "mailProvider.B.password}";
	private static final String MAIL_PROVIDER_B_ENDPOINT = "${" + PARAM_ROOT + "mailProvider.B.password}";

	@Value( MAIL_PROVIDER_A_USERNAME )
	private String mailProviderAUsername;

	@Value( MAIL_PROVIDER_A_PASSWORD )
	private String mailProviderAPassword;

	@Value( MAIL_PROVIDER_A_ENDPOINT )
	private String mailProviderAEndpoint;

	@Value( MAIL_PROVIDER_B_USERNAME )
	private String mailProviderBUsername;

	@Value( MAIL_PROVIDER_B_PASSWORD )
	private String mailProviderBPassword;

	@Value( MAIL_PROVIDER_B_ENDPOINT )
	private String mailProviderBEndpoint;

	private MailProviderVo mailProviderA;
	private MailProviderVo mailProviderB;

	@PostConstruct
	public void init()
	{
		mailProviderA = new MailProviderVo( mailProviderAUsername, mailProviderAPassword, mailProviderAEndpoint );
		mailProviderB = new MailProviderVo( mailProviderBUsername, mailProviderBPassword, mailProviderBEndpoint );
	}

	@Override
	public MailProviderVo getMailProviderA()
	{
		return mailProviderA;
	}

	@Override
	public MailProviderVo getMailProviderB()
	{
		return mailProviderB;
	}
}
