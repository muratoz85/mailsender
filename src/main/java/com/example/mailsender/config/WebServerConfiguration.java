package com.example.mailsender.config;

import com.example.mailsender.config.intf.IWebServerConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import static com.example.mailsender.constants.PropertyParameterNameRoot.PARAM_ROOT;

@Configuration
public class WebServerConfiguration implements IWebServerConfiguration
{
	private static final String APPLICATION_URL = "${" + PARAM_ROOT + "applicationURL}";

	@Value( APPLICATION_URL )
	private String applicationURL;

	@Override
	public String getApplicationURL()
	{
		return applicationURL;
	}
}
