package com.example.mailsender.config.intf;

import com.example.mailsender.vo.MailProviderVo;

public interface IMailProviderConfiguration
{
	MailProviderVo getMailProviderA();

	MailProviderVo getMailProviderB();
}
