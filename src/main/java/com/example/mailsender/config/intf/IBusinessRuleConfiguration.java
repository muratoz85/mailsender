package com.example.mailsender.config.intf;

public interface IBusinessRuleConfiguration
{
	Integer getSendTrialCountThreshold();
}
