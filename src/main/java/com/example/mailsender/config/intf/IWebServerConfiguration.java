package com.example.mailsender.config.intf;

public interface IWebServerConfiguration
{
	String getApplicationURL();
}
