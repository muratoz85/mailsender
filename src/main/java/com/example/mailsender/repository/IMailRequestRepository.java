package com.example.mailsender.repository;

import com.example.mailsender.entity.MailRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMailRequestRepository extends CrudRepository<MailRequest, Integer>
{
	@Query( "SELECT M FROM MailRequest M WHERE M.id > ?1 ORDER BY ID ASC" )
	List<MailRequest> getRecordsIdBiggerThanFloorIdOrderById( Integer floorId );
}
