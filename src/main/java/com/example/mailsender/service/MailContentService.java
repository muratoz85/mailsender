package com.example.mailsender.service;

import com.example.mailsender.constants.enums.MailType;
import com.example.mailsender.entity.MailParameter;
import com.example.mailsender.entity.MailRequest;
import com.example.mailsender.service.intf.IMailContentService;
import com.example.mailsender.vo.MailContentVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MailContentService implements IMailContentService
{
	@Override
	public MailContentVo createMailContent( MailRequest mailRequest )
	{
		MailType mailType = mailRequest.getType();
		MailContentVo mailContent = new MailContentVo( mailType.template.title, mailType.template.content );

		List<MailParameter> mailParameters = mailRequest.getParameters();
		if ( mailParameters != null )
		{
			mailParameters.forEach( mailParameter ->
									{
										replaceMailContentParam( mailContent, mailParameter );
									} );
		}

		return mailContent;
	}

	private void replaceMailContentParam( MailContentVo mailContent, MailParameter mailParameter )
	{
		String title = mailContent.getTitle();
		String content = mailContent.getContent();

		String parameter = mailParameter.getType().parameter;
		String value = mailParameter.getValue();

		title = title.replace( parameter, value );
		content = content.replace( parameter, value );

		mailContent.setTitle( title );
		mailContent.setContent( content );
	}
}
