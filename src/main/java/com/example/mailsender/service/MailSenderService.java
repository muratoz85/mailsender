package com.example.mailsender.service;

import com.example.mailsender.config.intf.IBusinessRuleConfiguration;
import com.example.mailsender.vo.MailVo;
import com.example.mailsender.externalsystem.intf.IMailServerMock;
import com.example.mailsender.service.intf.IMailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService implements IMailSenderService
{
	@Autowired
	private IBusinessRuleConfiguration businessRuleConfiguration;

	@Autowired
	private IMailServerMock mailServerMock;

	@Override
	public void sendMail( MailVo mail )
	{
		for ( int i = 0; i < businessRuleConfiguration.getSendTrialCountThreshold(); i++ )
		{
			if ( mailServerMock.sendMail( mail ) )
			{
				break;
			}
		}
	}
}
