package com.example.mailsender.service.intf;

import com.example.mailsender.entity.MailRequest;
import com.example.mailsender.vo.MailContentVo;

public interface IMailContentService
{
	MailContentVo createMailContent( MailRequest mailRequest );
}
