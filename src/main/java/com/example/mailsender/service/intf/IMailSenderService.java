package com.example.mailsender.service.intf;

import com.example.mailsender.vo.MailVo;

public interface IMailSenderService
{
	void sendMail( MailVo mail );
}
