package com.example.mailsender.service.intf;

import com.example.mailsender.vo.MailVo;
import com.example.mailsender.entity.MailRequest;

import java.util.List;

public interface IMailRequestService
{
	List<MailRequest> getAllUnprocessed(Integer idFloor );

	MailVo transformMailRequestToMail( MailRequest mailRequest );
}
