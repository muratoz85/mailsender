package com.example.mailsender.service;

import com.example.mailsender.vo.MailVo;
import com.example.mailsender.entity.MailRequest;
import com.example.mailsender.vo.builder.MailBuilder;
import com.example.mailsender.repository.IMailRequestRepository;
import com.example.mailsender.service.intf.IMailContentService;
import com.example.mailsender.service.intf.IMailRequestService;
import com.example.mailsender.vo.MailContentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MailRequestService implements IMailRequestService
{
	@Autowired
	private IMailRequestRepository mailRequestRepository;

	@Autowired
	private IMailContentService mailContentService;

	@Override
	public List<MailRequest> getAllUnprocessed(Integer idFloor )
	{
		return mailRequestRepository.getRecordsIdBiggerThanFloorIdOrderById( idFloor );
	}

	public MailVo transformMailRequestToMail( MailRequest mailRequest )
	{
		MailContentVo mailContent = mailContentService.createMailContent( mailRequest );

		return new MailBuilder().setProvider( mailRequest.getProvider() )
				.setReceiver( mailRequest.getReceiver() )
				.setCc( mailRequest.getCc() )
				.setTopic( mailContent.getTitle() )
				.setContent( mailContent.getContent() )
				.build();
	}
}
