package com.example.mailsender.externalsystem;

import com.example.mailsender.config.intf.IMailProviderConfiguration;
import com.example.mailsender.constants.enums.MailProviderType;
import com.example.mailsender.externalsystem.intf.IMailServerMock;
import com.example.mailsender.vo.MailProviderVo;
import com.example.mailsender.vo.MailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MailServerMock implements IMailServerMock
{
	@Autowired
	private IMailProviderConfiguration mailProviderConfiguration;

	@Override
	public boolean sendMail( MailVo mail )
	{
		MailProviderVo mailProvider = getMailProviderInfo( mail.getProvider() );
		boolean success = mailProvider != null;

		StringBuilder stringBuilder = getMailAsPrintable( mail );
		stringBuilder.append( success ? "Mail sent!\n" : "Mail sending failed!\n" );
		System.out.println( stringBuilder );

		return success;
	}

	private StringBuilder getMailAsPrintable( MailVo mail )
	{
		MailProviderType provider = mail.getProvider();
		return new StringBuilder().append( "\nPROVIDER: " )
				.append( provider.name() )
				.append( "\nTO: " )
				.append( mail.getProvider() )
				.append( "\nCC: " )
				.append( mail.getCc() )
				.append( "\nTOPIC: " )
				.append( mail.getTopic() )
				.append( "\nCONTENT: " )
				.append( mail.getContent() )
				.append( "\n" );
	}

	private MailProviderVo getMailProviderInfo( MailProviderType mailProviderType )
	{
		switch ( mailProviderType )
		{
		case A:
			return mailProviderConfiguration.getMailProviderA();
		case B:
			return mailProviderConfiguration.getMailProviderB();
		default:
			return null;
		}
	}
}
