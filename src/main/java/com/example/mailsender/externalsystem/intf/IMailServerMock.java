package com.example.mailsender.externalsystem.intf;

import com.example.mailsender.vo.MailVo;

public interface IMailServerMock
{
	boolean sendMail(MailVo mail );
}
