package com.example.mailsender.vo;

public class MailProviderVo
{
	private final String username;
	private final String password;
	private final String endpoint;

	public MailProviderVo( String username, String password, String endpoint )
	{
		this.username = username;
		this.password = password;
		this.endpoint = endpoint;
	}

	public String getUsername()
	{
		return username;
	}

	public String getPassword()
	{
		return password;
	}

	public String getEndpoint()
	{
		return endpoint;
	}
}
