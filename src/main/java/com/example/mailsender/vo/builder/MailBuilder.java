package com.example.mailsender.vo.builder;

import com.example.mailsender.constants.enums.MailProviderType;
import com.example.mailsender.vo.MailVo;

public class MailBuilder
{
	private Integer id;
	private MailProviderType provider;
	private String receiver;
	private String cc;
	private String topic;
	private String content;

	public MailBuilder setProvider( MailProviderType provider )
	{
		this.provider = provider;
		return this;
	}

	public MailBuilder setReceiver( String receiver )
	{
		this.receiver = receiver;
		return this;
	}

	public MailBuilder setCc( String cc )
	{
		this.cc = cc;
		return this;
	}

	public MailBuilder setTopic( String topic )
	{
		this.topic = topic;
		return this;
	}

	public MailBuilder setContent( String content )
	{
		this.content = content;
		return this;
	}

	public MailVo build()
	{
		MailVo mail = new MailVo();
		mail.setProvider( provider );
		mail.setReceiver( receiver );
		mail.setCc( cc );
		mail.setTopic( topic );
		mail.setContent( content );

		return mail;
	}
}
