package com.example.mailsender.vo;

import com.example.mailsender.constants.enums.MailProviderType;

public class MailVo
{
	private MailProviderType provider;
	private String receiver;
	private String cc;
	private String topic;
	private String content;

	public MailProviderType getProvider()
	{
		return provider;
	}

	public void setProvider( MailProviderType provider )
	{
		this.provider = provider;
	}

	public String getReceiver()
	{
		return receiver;
	}

	public void setReceiver( String receiver )
	{
		this.receiver = receiver;
	}

	public String getCc()
	{
		return cc;
	}

	public void setCc( String cc )
	{
		this.cc = cc;
	}

	public String getTopic()
	{
		return topic;
	}

	public void setTopic( String topic )
	{
		this.topic = topic;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent( String content )
	{
		this.content = content;
	}
}
