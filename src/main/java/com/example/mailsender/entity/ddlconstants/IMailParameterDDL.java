package com.example.mailsender.entity.ddlconstants;

public interface IMailParameterDDL
{
	String TABLE = "MAIL_PARAMETER";
	String ID = "ID";
	String MAIL_REQUEST_ID = "MAIL_REQUEST_ID";
	String TYPE = "TYPE";
	String VALUE = "VALUE";

	String MAIL_REQUEST_JOIN_FIELD = "mailRequest";
}
