package com.example.mailsender.entity.ddlconstants;

public interface IMailRequestDDL
{
	String TABLE = "MAIL_REQUEST";
	String ID = "ID";
	String PROVIDER = "PROVIDER";
	String RECEIVER = "RECEIVER";
	String CC = "CC";
}
