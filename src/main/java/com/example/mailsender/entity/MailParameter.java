package com.example.mailsender.entity;

import com.example.mailsender.entity.ddlconstants.IMailParameterDDL;
import com.example.mailsender.constants.enums.MailParameterType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = MailParameter.TABLE )
public class MailParameter implements IMailParameterDDL
{
	@Id
	@Column( name = ID )
	private Integer id;

	@ManyToOne( optional = false )
	@JoinColumn( name = MAIL_REQUEST_ID )
	private MailRequest mailRequest;

	@Column( name = TYPE, nullable = false )
	@Enumerated( EnumType.STRING )
	private MailParameterType type;

	@Column( name = VALUE, nullable = false )
	private String value;

	public Integer getId()
	{
		return id;
	}

	public void setId( Integer id )
	{
		this.id = id;
	}

	public MailRequest getMailRequest()
	{
		return mailRequest;
	}

	public void setMailRequest( MailRequest mailRequest )
	{
		this.mailRequest = mailRequest;
	}

	public MailParameterType getType()
	{
		return type;
	}

	public void setType( MailParameterType type )
	{
		this.type = type;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue( String value )
	{
		this.value = value;
	}
}
