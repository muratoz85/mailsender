package com.example.mailsender.entity;

import com.example.mailsender.entity.ddlconstants.IMailRequestDDL;
import com.example.mailsender.constants.enums.MailProviderType;
import com.example.mailsender.constants.enums.MailType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table( name = MailRequest.TABLE )
public class MailRequest implements IMailRequestDDL
{
	@Id
	@Column( name = ID )
	private Integer id;

	@Column( name = PROVIDER, nullable = false )
	@Enumerated( EnumType.STRING )
	private MailProviderType provider;

	@Column
	@Enumerated( EnumType.STRING )
	private MailType type;

	@Column( name = RECEIVER, nullable = false )
	private String receiver;

	@Column( name = CC )
	private String cc;

	@OneToMany( mappedBy = MailParameter.MAIL_REQUEST_JOIN_FIELD, fetch = FetchType.EAGER)
	private List<MailParameter> parameters;

	public Integer getId()
	{
		return id;
	}

	public void setId( Integer id )
	{
		this.id = id;
	}

	public MailProviderType getProvider()
	{
		return provider;
	}

	public void setProvider( MailProviderType provider )
	{
		this.provider = provider;
	}

	public MailType getType()
	{
		return type;
	}

	public void setType( MailType type )
	{
		this.type = type;
	}

	public String getReceiver()
	{
		return receiver;
	}

	public void setReceiver( String receiver )
	{
		this.receiver = receiver;
	}

	public String getCc()
	{
		return cc;
	}

	public void setCc( String cc )
	{
		this.cc = cc;
	}

	public List<MailParameter> getParameters()
	{
		return parameters;
	}

	public void setParameters( List<MailParameter> parameters )
	{
		this.parameters = parameters;
	}
}
