package com.example.mailsender.jobs;

import com.example.mailsender.service.intf.IMailSenderService;
import com.example.mailsender.vo.MailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Vector;

@Component
public class MailSenderJob
{
	private static final long FIXED_RATE = 10000;

	private volatile Vector<MailVo> mailQueue = new Vector<>();

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private IMailSenderService mailSenderService;

	public synchronized void queueMails( Vector<MailVo> newMails )
	{
		mailQueue.addAll( newMails );
	}

	@Scheduled( fixedRate = FIXED_RATE )
	protected void processMailRequests()
	{
		try
		{
			processMailQueue();
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}

	private void processMailQueue()
	{
		while ( !mailQueue.isEmpty() )
		{
			sendMail( mailQueue.remove( 0 ) );
		}
	}

	private void sendMail( MailVo mail )
	{
		taskExecutor.execute( createMailSenderRunnable( mail ) );
	}

	private Runnable createMailSenderRunnable( MailVo mail )
	{
		return () ->
		{
			try
			{
				mailSenderService.sendMail( mail );
			}
			catch ( Throwable t )
			{
				t.printStackTrace();
			}
		};
	}
}
