package com.example.mailsender.jobs;

import com.example.mailsender.entity.MailRequest;
import com.example.mailsender.service.intf.IMailRequestService;
import com.example.mailsender.vo.MailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Vector;

@Component
public class MailRequestProcessorJob
{
	private static final long FIXED_RATE = 10000;

	private volatile Vector<MailRequest> mailRequests = new Vector<>();
	private volatile Vector<MailVo> mails = new Vector<>();

	private volatile int maxId = 0;

	@Autowired
	private IMailRequestService mailRequestService;

	@Autowired
	private MailSenderJob mailSenderJob;

	private void readMailRequests()
	{
		List<MailRequest> newMailRequests = mailRequestService.getAllUnprocessed( maxId );
		if ( newMailRequests != null && !newMailRequests.isEmpty() )
		{
			mailRequests.addAll( newMailRequests );
		}
	}

	private void transformMailsRequestsToMails()
	{
		while ( !mailRequests.isEmpty() )
		{
			MailRequest mailRequest = mailRequests.remove( 0 );
			MailVo mail = mailRequestService.transformMailRequestToMail( mailRequest );
			mails.add( mail );
			maxId = mailRequest.getId();
		}
	}

	private void sendMails()
	{
		while ( !mails.isEmpty() )
		{
			mailSenderJob.queueMails( mails );
			mails.clear();
		}
	}

	@Scheduled( fixedRate = FIXED_RATE )
	protected void processMailRequests()
	{
		try
		{
			readMailRequests();
			transformMailsRequestsToMails();
			sendMails();
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}
}
