DB initialization script is the "init.sql" file under the sql folder.

The queue is assumed to the MAIL_REQUEST table.
A record having a smaller id is assumed as a first-comer.

Mail providers' data is gathered from "application.properties" file.
The beans under the "jobs" package are initiaters.

The number of email sending trials before failure is also gathered from "application.properties" file.

Email providers are represented with the MailServerMock bean under the "externalsystem" package.
The failure condition for email sending is assumed as mailRequest having an invalid mailProvider.

"constants.enum" package requires special attention.

P.S: Unit tests are omitted because the developer's health conditions restricted his time.
Same reason also caused the delay. Sorry for that.